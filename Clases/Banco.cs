﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bancomer.Clases
{
    class Banco
    {
        //public Cliente cliente1 { get; set; }
        //public Cliente cliente2 { get; set; }
        //public Cliente cliente3 { get; set; }

        public List<Cliente> clientes { get; set; }

        public Banco()
        {
            //cliente1 = new Cliente();
            //cliente2 = new Cliente();
            //cliente3 = new Cliente();

            clientes = new List<Cliente>();
        }

        public void AgregarCliente()
        {
            Console.WriteLine("Nuevo cliente");
            Console.Write("Nombre: ");
            Cliente cliente = new Cliente();
            cliente.Nombre = Console.ReadLine();
            clientes.Add(cliente);
            Console.WriteLine("Cliente agregado exitosamente");
            Depositar(cliente);
        }

        public void Depositar(Cliente cliente)
        {
            Console.Write("Cantidad: ");

            try
            {
                double cantidad = double.Parse(Console.ReadLine());
                cliente.Depositar(cantidad);
            }
            catch (Exception e)
            {
                Console.WriteLine("Cantidad incorrecta intente de nuevo");
                Depositar(cliente);
            }
        }

        public double Monto
        {
            get
            {
                //double monto = cliente1.Monto + cliente2.Monto + cliente3.Monto;
                double monto = clientes.Sum(x => x.Monto);
                return monto;
            }
        }

        public void Procesar()
        {
            //cliente1.Nombre = "Johan";
            //cliente2.Nombre = "Hau Pech";
            //cliente3.Nombre = "Fierro";

            //cliente1.Depositar(100);
            //cliente2.Depositar(200);
            //cliente3.Depositar(300);

            while (true)
            {
                Console.WriteLine("Menu de opciones:");
                Console.WriteLine("1) Agregar cliente");
                Console.WriteLine("2) Hacer corte");

                string opcion = Console.ReadLine();
                if (opcion == "1")
                {
                    AgregarCliente();
                }
                else if (opcion == "2")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Seleciona una opcion del menu");
                }
            }

            DateTime t = DateTime.Now;
            Console.WriteLine($"Hora de corte: {t.Hour}:{t.Minute}, monto: {Monto}");
        }
    }
}
