﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bancomer.Clases
{
    class Cliente
    {
        public string Nombre { get; set; }

        private double monto = 0;
        public double Monto
        {
            get
            {
                return monto;
            }
        }

        private void Modificar(double cantidad)
        {
            monto = monto + cantidad;
            Console.WriteLine($"El cliente {Nombre} tiene: {Monto}");
        }
        public void Depositar(double cantidad)
        {
            Modificar(cantidad);
        }

        public void Retirar(double cantidad)
        {
            if (monto >= cantidad && cantidad > 0)
            {
                Modificar(-cantidad);
            }
            else
            {
                Console.WriteLine("Dinero insuficiente para el retiro");
            }
        }
    }
}
