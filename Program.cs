﻿using Bancomer.Clases;
using System;

namespace Bancomer
{
    class Program
    {
        static void Main(string[] args)
        {
            Banco banco = new Banco();
            banco.Procesar();
        }
    }
}
